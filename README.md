REST API using Express 4


This API can be accessed through any clients 

A.Steps to set Up the Project

1.Clone the project using bitbucket url of contacts_app

2.run the command npm install , to install the npm modules

3.run the command npm start,this command will start the server at http://127.0.0.1:4000/ and also will run the function that is neccessary to create the keyspace and table in cassandra

4.The Keyspace used is People and the table used is contacts that has four fields-id,name,email,phone

B.API URL'S

1.Get List of contacts
 URL:http://127.0.0.1:4000/test/
 Type:GET
  
It is used to get all the contacts from cassandra database


2. Add a contact
URL:http://127.0.0.1:4000/test/
   Type:POST
   Body:
   use x-www-form-urlencoded
   id:1,name:'xyz',email:'xyz@gmail.com',phone:'1234567890'
   
3. Search With Name
   URL:http://127.0.0.1:4000/search/xyz
   Type:Get
   Note:xyz is the name you would like to search that persist in the database